import 'package:cards_against_spil_stuff/picker_page.dart';
import 'package:cards_against_spil_stuff/reader_page.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';

import 'dart:math';

String readerUser = '';
int readerIndex = 0;

class FindLobbyWidget extends StatefulWidget {
  @override

  final String username;
  FindLobbyWidget({required this.username});

  _FindLobbyWidgetState createState() => _FindLobbyWidgetState();
}

class _FindLobbyWidgetState extends State<FindLobbyWidget> {
  final DatabaseReference usersRef =
  FirebaseDatabase.instance.reference().child('users');

  List<String> users = [];
  Random random = Random();

  @override
  void initState() {
    super.initState();
    listenForUsernames();
  }

  void listenForUsernames() {
    usersRef.onValue.listen((event) {
      final dataSnapshot = event.snapshot;
      final userList = dataSnapshot.value as List<dynamic>;

      setState(() {
        users = List<String>.from(userList);
      });
    });
  }

  void addUser(String username) {
    if (!users.contains(username)) {
      users.add(username);
      usersRef.set(users);
    }
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
        children: [
          SizedBox(height: 80,),
          ElevatedButton(
            onPressed: () {
              String username = widget.username;
              addUser(username);
            },
            child: Text(
              'Join',
              style: TextStyle(
                fontSize: 24,
              )
            ),
          ),
          SizedBox(height: 20),
          Text(
            'Users in the lobby:',
            style: TextStyle(
              fontSize: 26
            ),
          ),
          SizedBox(height: 10),
          Expanded(
            child: ListView.builder(
              itemCount: users.length,
              itemBuilder: (context, index) {
                  return Center(
                    child: Column(
                      children: [
                        SizedBox(height: 10),
                        Text(
                          users[index],
                          style: TextStyle(
                            fontSize: 20,
                          )
                        )
                      ],
                    )
                  );
              },
            ),
          ),

          ElevatedButton(
            onPressed: () {
              if (users.length >= 3){
                readerUser = users[readerIndex];//random.nextInt(users.length)];
                if (readerUser != widget.username){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => PickerPage(
                      users: users,
                      username: widget.username,
                    )),
                  );
                } else{
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ReaderPage(
                      users: users,
                      username: widget.username,
                      chosenCard: ''
                    )),
                  );
                }
              }
            },
            child: Text(
              'Start',
              style: TextStyle(
                fontSize: 24,
              )
            ),
          ),
        ],
        ),
      ),
    );
  }
}