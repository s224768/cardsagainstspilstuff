import 'dart:math';

import 'package:cards_against_spil_stuff/find_match.dart';
import 'package:cards_against_spil_stuff/reader_page.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'cards.dart';

class PickerPage extends StatefulWidget {
  @override
  
  final List<String> users;
  final String username;
  
  PickerPage({
    required this.users,
    required this.username
  });

  PickerPageState createState() => PickerPageState();
}

class PickerPageState extends State<PickerPage> {
  final blackCardRef = FirebaseDatabase.instance.reference().child('Blackcard');
  final whiteCardRef = FirebaseDatabase.instance.reference().child('Whitecard');
  final pointRef = FirebaseDatabase.instance.reference().child('Points');
  final choiceRef = FirebaseDatabase.instance.reference().child('UserChoice');
  final readerUserRef = FirebaseDatabase.instance.reference().child('index');

  Random random = Random();

  String blackcard = '';
  int readerUserIndex = 0;
  List<String> whitecard = [];
  List<int> point = [];
  List<String> choice = [];

  int selectedCard = -1;

  List<String> cards = [];

  void initState() {
    super.initState();
    listenForChanges();

    if (point.length < widget.users.length){
      int _length = point.length;
      for(int i = 0; i < widget.users.length - _length; i += 1){
        point.add(0);
      }
    }
  }

  void listenForChanges() {
    blackCardRef.onValue.listen((event) {
      if (event.snapshot.value != null) {
        final data = event.snapshot.value as String?;
        setState(() {
          blackcard = data as String;
        });
      }
    });

    whiteCardRef.onValue.listen((event) {
    if (event.snapshot.value != null) {
      final data = event.snapshot.value as List<dynamic>?;
      final List<String> cardst = data?.map((item) => item.toString()).toList() ?? [];
      setState(() {
        whitecard = cardst;
      });
    }
    });

    pointRef.onValue.listen((event) {
      if (event.snapshot.value != null) {
        final data = event.snapshot.value as List<dynamic>?;
        final List<int> cards = data?.map((item) => int.parse(item.toString())).toList() ?? [];
        setState(() {
          point = cards;
        });
      }
    });
  
    choiceRef.onValue.listen((event) {
      if (event.snapshot.value != null) {
        final data = event.snapshot.value as List<dynamic>?;
        final List<String> cards = data?.map((item) => (item.toString())).toList() ?? [];
        setState(() {
          choice = cards;
        });
      }
    });

    readerUserRef.onValue.listen((event) {
      if (event.snapshot.value != null) {
        final data = event.snapshot.value as int?;
        setState(() {
          readerUserIndex = data as int;
          readerUserIndex = readerUserIndex % (widget.users.length);
        });
      }
    });
  }


  Widget build(BuildContext context) {
    for(int i = 0; i < 10 - cards.length; i += 1){
      cards.add(whiteCards[random.nextInt(whiteCards.length)]);
    }

    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      
      child: Scaffold(
        body: Column(
          children: [
            SizedBox(height: 20),
            Stack(
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: SizedBox(
                  width: 160,
                  height: 160,
                  child: Card(
                    color: Colors.black,
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                        blackcard,
                        style: TextStyle(fontSize: 20, color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 50,
                right: 20,
                child: SizedBox(
                  child: ElevatedButton(
                    onPressed: () { 
                      if (selectedCard != -1){
                        whitecard.add(cards[selectedCard]);
                        whiteCardRef.set(whitecard);
                        choice.add(widget.username);
                        choiceRef.set(choice);
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => ReaderPage(
                            users: widget.users,
                            username: widget.username, 
                            chosenCard: cards[selectedCard],
                          )),
                        );
                      }
                    },
                    child: Text('Confirm')
                  ),
                ),
              ),
              Positioned(
                top: 20,
                right: 150,
                child: SizedBox(
                  width: 80,
                  height: 80,
                  child: ListView.builder(
                    itemCount: widget.users.length,
                    itemBuilder: (context, index) {
                        return Text('${widget.users[index]}: ${point[index]}');
                    },
                  ),
                ),
              )
            ],
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: List.generate(cards.length, (index) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          selectedCard = index;
                        });
                      },
                      child: Card(
                        color: selectedCard == index ? Colors.grey : Colors.white,
                        child: SizedBox(
                          width: 160,
                          height: 160,
                          child: Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(
                                cards[index],
                                style: TextStyle(fontSize: 16, color: Colors.black),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  );
                }),
              ),
            ),
          ],
        ),
      ),
    );
  }
}