import 'dart:math';

import 'package:cards_against_spil_stuff/find_match.dart';
import 'package:cards_against_spil_stuff/picker_page.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'cards.dart';

class ReaderPage extends StatefulWidget {
  @override
  
  final List<String> users;
  final String username;
  final String chosenCard;
  ReaderPage({
    required this.users,
    required this.username,
    required this.chosenCard
  });

  ReaderPageState createState() => ReaderPageState();
}

class ReaderPageState extends State<ReaderPage> {
  final blackCardRef = FirebaseDatabase.instance.reference().child('Blackcard');
  final whiteCardRef = FirebaseDatabase.instance.reference().child('Whitecard');
  final pointRef = FirebaseDatabase.instance.reference().child('Points');
  final choiceRef = FirebaseDatabase.instance.reference().child('UserChoice');
  final winCardRef = FirebaseDatabase.instance.reference().child('wincard');
  final readerUserRef = FirebaseDatabase.instance.reference().child('index');
  Random random = Random();

  String blackcard = '';
  int wincard = -1;
  List<String> whitecard = [];

  int selectedCard = -1; 
  int readerUserIndex = 0;

  List<int> point = [];
  List<String> cards = [];
  List<String> choice = [];

  void initState() {
    super.initState();
    listenForChanges();

    if (point.length < widget.users.length){
      int _length = point.length;
      for(int i = 0; i < widget.users.length - _length; i += 1){
        point.add(0);
      }
    }
  }

  void listenForChanges() {
    blackCardRef.onValue.listen((event) {
      if (event.snapshot.value != null) {
        final data = event.snapshot.value as String?;
        setState(() {
          blackcard = data as String;
        });
      }
    });

    whiteCardRef.onValue.listen((event) {
    if (event.snapshot.value != null) {
      final data = event.snapshot.value as List<dynamic>?;
      final List<String> cardst = data?.map((item) => item.toString()).toList() ?? [];
      setState(() {
        whitecard = cardst;
      });
    }
    });

    pointRef.onValue.listen((event) {
      if (event.snapshot.value != null) {
        final data = event.snapshot.value as List<dynamic>?;
        final List<int> cards = data?.map((item) => int.parse(item.toString())).toList() ?? [];
        setState(() {
          point = cards;
        });
      }
    });
  
    choiceRef.onValue.listen((event) {
      if (event.snapshot.value != null) {
        final data = event.snapshot.value as List<dynamic>?;
        final List<String> cards = data?.map((item) => (item.toString())).toList() ?? [];
        setState(() {
          choice = cards;
        });
      }
    });

    winCardRef.onValue.listen((event) {
      if (event.snapshot.value != null) {
        final data = event.snapshot.value as int?;
        setState(() {
          wincard = data as int;
        });
      }
    });

    readerUserRef.onValue.listen((event) {
      if (event.snapshot.value != null) {
        final data = event.snapshot.value as int?;
        setState(() {
          readerUserIndex = data as int;
          readerUserIndex = readerUserIndex % (widget.users.length-1);
        });
      }
    });
  }

  Widget build(BuildContext context) {
    if (wincard != -1){
      Future.delayed(Duration(seconds: 4), () {
      wincard = -1;
      readerUser = widget.users[readerUserIndex];
      if (readerUser != widget.username){
        whiteCardRef.remove();
        winCardRef.remove();
        choiceRef.remove();
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => PickerPage(
            users: widget.users,
            username: widget.username,
          )),
        );
      } else{
        whiteCardRef.remove();
        winCardRef.remove();
        choiceRef.remove();
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ReaderPage(
            users: widget.users,
            username: widget.username,
            chosenCard: ''
          )),
        );
      }
    });
    }

    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      
      child: Scaffold(
        body: Column(
          children: [
            SizedBox(height: 20),
            Stack(
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: SizedBox(
                  width: 160,
                  height: 160,
                  child: Card(
                    color: Colors.black,
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                        blackcard,
                        style: TextStyle(fontSize: 20, color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 50,
                right: 20,
                child: SizedBox(
                  child: ElevatedButton(
                    onPressed: () { 
                      if (selectedCard != -1 && readerUser == widget.users[readerUserIndex]){
                        readerUserIndex += 1;
                        readerUserIndex = readerUserIndex % widget.users.length;
                        readerUserRef.set(readerUserIndex);
                        point[widget.users.indexOf(choice[selectedCard])] += 1;
                        pointRef.set(point);
                        wincard = selectedCard;
                        winCardRef.set(wincard);
                      }
                    },
                    child: Text('Confirm')
                  ),
                ),
              ),
              Positioned(
                top: 20,
                right: 150,
                child: SizedBox(
                  width: 80,
                  height: 80,
                  child: ListView.builder(
                    itemCount: widget.users.length,
                    itemBuilder: (context, index) {
                        return Text('${widget.users[index]}: ${point[index]}');
                    },
                  ),
                ),
              )
            ]
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Stack(
                children: [
                Row(
                children: List.generate(whitecard.length, (index) {
                  print('F>MKLSNFJKFSNJKLSFNJKFLSKN');
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                      child: GestureDetector(
                        onTap: () {
                          if (widget.chosenCard == ''){
                          setState(() {
                            selectedCard = index;
                          });
                          }
                        },
                        child: Card(
                          color: (wincard != -1 && wincard == index) ? Colors.green : ((selectedCard == index) ? Colors.grey : Colors.white),
                          child: Align(
                          alignment: Alignment.centerLeft,
                          child: SizedBox(
                            width: 160,
                            height: 160,
                              child: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text(
                                  whitecard[index],
                                  style: TextStyle(fontSize: 16, color: Colors.black),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                  );
                }),
                ),
                ]
              ),
            ),
          ],
        ),
      ),
    );
  }
}